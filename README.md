OpenFL Spriter
==========

This library is for rendering Spriter characters using OpenFL. It
depends on the [haxe-spriter library][haxe-spriter]. You'll have to
install it from git until I submit it to haxelib.

    haxelib git https://bitbucket.org/ClockworkMagpie/haxe-spriter.git
    haxelib git https://bitbucket.org/ClockworkMagpie/openfl-spriter.git

[haxe-spriter]: https://bitbucket.org/ClockworkMagpie/haxe-spriter
