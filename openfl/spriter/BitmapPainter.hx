package openfl.spriter;

import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Point;
import spriter.ITimelineVisitor;
import spriter.SpatialInfo;

class BitmapPainter implements ITimelineVisitor
{
    private var mCanvas : BitmapData;
    private var mPoint : Point;
    private var mMatrix : Matrix;
    
    public var characterInfo : SpatialInfo;

    public function new(canvas : BitmapData)
    {
        mCanvas = canvas;
        mPoint = new Point();
        mMatrix = new Matrix();
        
        characterInfo = new SpatialInfo();
    }

    public function clear(color : Int = 0)
    {
        mCanvas.fillRect(mCanvas.rect, color);
    }

    public function setPosition(x : Float, y : Float)
    {
        characterInfo.x = x;
        characterInfo.y = mCanvas.height - y;
    }

    public function sprite(fileRef : Dynamic,
                           cx : Float, cy : Float,
                           scaleX : Float, scaleY : Float,
                           angle : Float,
                           offsetX : Float, offsetY : Float)
    {
        var bmp : BitmapData = cast fileRef;
        cy = mCanvas.height - cy;

        // The offset uses UV-coordinates
        offsetX *= bmp.width;
        offsetY = bmp.height * (1 - offsetY);

        if(angle == 0 && scaleX == 1 && scaleY == 1)
        {
            mPoint.x = cx - offsetX;
            mPoint.y = cy - offsetY;
            mCanvas.copyPixels(bmp, bmp.rect, mPoint);
        }
        else
        {
            mMatrix.identity();
            mMatrix.translate(-offsetX,
                              -offsetY);
            mMatrix.scale(scaleX, scaleY);
            mMatrix.rotate(-angle);
            mMatrix.translate(cx, cy);
            mCanvas.draw(bmp, mMatrix, null, null, null, true);
        }
    }
}
