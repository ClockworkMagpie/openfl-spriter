package openfl.spriter;

import flash.display.Graphics;
import de.polygonal.ds.ArrayUtil;
import openfl.display.Tilesheet;
import spriter.ITimelineVisitor;
import spriter.SpatialInfo;

class TilesheetPainter implements ITimelineVisitor
{
    private var mSheet : Tilesheet;
    private var mTiles : Array<Float>;
    private var mHeight : Float;
    private var mCount : Int;
    
    public var characterInfo : SpatialInfo;

    public function new(sheet : Tilesheet, height : Float)
    {
        mSheet = sheet;
        mTiles = [];
        mHeight = height;
        characterInfo = new SpatialInfo();
    }

    public function begin()
    {
        mCount = 0;
    }

    public function draw(gr : Graphics)
    {
        if(mTiles.length > mCount)
        {
            ArrayUtil.shrink(mTiles, mCount);
        }
        mSheet.drawTiles(gr, mTiles, true, Tilesheet.TILE_TRANS_2x2);
    }

    public function sprite(fileRef : Dynamic,
                           cx : Float, cy : Float,
                           scaleX : Float, scaleY : Float,
                           angle : Float,
                           offsetX : Float, offsetY : Float)
    {
        var tileInfo : TileInfo = fileRef;
        
        cy = mHeight - cy;

        // The offset uses UV-coordinates
        offsetX *= tileInfo.width;
        offsetY = tileInfo.height * (1 - offsetY);

        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        var sox = scaleX * offsetX;
        var soy = scaleY * offsetY;

        mTiles[mCount++] = cx - sox * cos - soy * sin;
        mTiles[mCount++] = cy + sox * sin - soy * cos;
        mTiles[mCount++] = tileInfo.id;
        mTiles[mCount++] = scaleX * cos;
        mTiles[mCount++] = -scaleX * sin;
        mTiles[mCount++] = scaleY * sin;
        mTiles[mCount++] = scaleY * cos;
    }
}
