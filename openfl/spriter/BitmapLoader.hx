package openfl.spriter;

import spriter.IImageLoader;

class BitmapLoader implements IImageLoader
{
    private var mBase : String;
    
    public function new(basePath : String)
    {
        mBase = basePath;
    }

    public function loadImage(relativePath : String) : Dynamic
    {
        return openfl.Assets.getBitmapData(mBase + "/" + relativePath);
    }
}

