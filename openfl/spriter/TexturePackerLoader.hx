package openfl.spriter;

import flash.geom.Rectangle;
import openfl.display.Tilesheet;
import spriter.IImageLoader;

class TexturePackerLoader implements IImageLoader
{
    public var sheet : Tilesheet;
    private var mSprites : Map<String, TileInfo>;
    
    public function new(sheetPath : String, data : String)
    {
        sheet = new Tilesheet(openfl.Assets.getBitmapData(sheetPath));
        mSprites = new Map<String, TileInfo>();
        
        var json = haxe.Json.parse(openfl.Assets.getText(data));
        for(frame in cast(json.frames, Array<Dynamic>))
        {
            var x = Std.parseInt(frame.frame.x);
            var y = Std.parseInt(frame.frame.y);
            var width = Std.parseInt(frame.frame.w);
            var height = Std.parseInt(frame.frame.h);
            var info = new TileInfo();
            info.id = sheet.addTileRect(new Rectangle(x, y, width, height));
            info.width = width;
            info.height = height;
            mSprites.set(frame.filename, info);
        }
    }
    
    public function loadImage(relativePath : String) : Dynamic
    {
        return mSprites[relativePath];
    }
}
